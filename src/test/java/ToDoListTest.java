import com.sun.tools.javac.comp.Todo;
import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {

    @Test
    public void testAddToList() {
        String expectedTodo ="1. Do Dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [NOT DONE]";

        ToDoList ToDoListTask = new ToDoList();

        ToDoListTask.addToDoList("Do Dishes", "DONE", 1);
        ToDoListTask.addToDoList("Learn Java", "NOT DONE", 2);
        ToDoListTask.addToDoList("Learn TDD", "NOT DONE", 3);
        String actual = ToDoListTask.getToDoListData();

        assertEquals(expectedTodo,actual );
    }
    @Test
    public void testGetToDoListDataOnNullReferences(){
        ToDoList todo = new ToDoList();

        assertNull(todo.getToDoListData());
    }

    @Test
     public void tesChangeStatus(){

        String expectedTodo ="1. Do Dishes [DONE]\n2. Learn Java [NOT DONE]\n3. Learn TDD [DONE]";

        ToDoList ToDoListTask = new ToDoList();

        ToDoListTask.addToDoList("Do Dishes", "DONE", 1);
        ToDoListTask.addToDoList("Learn Java", "NOT DONE", 2);
        ToDoListTask.addToDoList("Learn TDD", "NOT DONE", 3);
        ToDoListTask.changeStatus(3);
        String actual = ToDoListTask.getToDoListData();

        assertEquals(expectedTodo,actual);


    }

}