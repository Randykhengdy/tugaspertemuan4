import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {

    @Test
    public void addTaskList() {
        String expectedTodo ="1. Do Dishes [Done]";

        Task task = new Task("Do Dishes", "Done", 1);

        assertEquals(expectedTodo, task.getTaskData());
    }

    @Test
    public void checkTaskDataIsNull() {
        Task task = new Task();

        //assertEquals(null, task.getTaskData());
        assertNull(task.getTaskData());
    }


    @Test
    public void checkTaskStatusIsDoneOrNotDone(){

        Task task = new Task("Do Dishes", "DONE", 1);
        boolean actual = task.CheckTaskStatus();
        assertTrue(actual);

    }
}