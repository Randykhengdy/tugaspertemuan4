import java.util.ArrayList;

public class ToDoList {


    private ArrayList<Task> toDoLists = new ArrayList<>();

    public ToDoList(){

    }
    public void addToDoList(String name, String status, int id) {

        Task task = new Task(name,status,id);

        toDoLists.add(task);

    }
    public String getToDoListData() {

        StringBuffer tempList = new StringBuffer();
        if (toDoLists.size()==0){
            return null;
        }else{
            for(int i=0;i<toDoLists.size();i++){
                if(i<toDoLists.size()-1){
                    tempList.append(toDoLists.get(i).getTaskData() + "\n");
                }else{
                    tempList.append(toDoLists.get(i).getTaskData()) ;
                }
            }
            return tempList.toString();
        }


    }

    void changeStatus(int id){
        Task taskTemp;

        taskTemp = toDoLists.get(id-1);

        taskTemp.setStatus("DONE");

        toDoLists.set(id-1,taskTemp);
    }






}
