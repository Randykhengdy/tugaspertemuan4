public class Task {
    private String name;
    private String status;
    private int id;

    public Task() {

    }
    public Task(String name, String status, int id) {
        this.name = name;
        this.status = status;
        this.id = id;
    }


    public String getTaskData() {
        if(name==null){
            return null;
        }
        else{
            return this.id + ". " + this.name + " [" + this.status + "]";
        }
    }

    public Boolean CheckTaskStatus() {
        return status.equals("DONE")  || status.equals("NOT DONE") ;
    }


    public void setStatus(String status) {
        this.status = status;
    }
}
